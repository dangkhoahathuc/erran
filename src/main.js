const { app, BrowserWindow, dialog, ipcMain, Menu } = require('electron')
const path = require('path')

async function loadFiles(){
  const {cancelled, paths} = await dialog.showOpenDialog()
  if(cancelled) return
  else return paths
}

function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preload.js')
    }
  })
  //Set up Application Menu
  const isMac = process.platform ==="darwin";
  const menuTemplate = [
      {
        label: "File",
        submenu: [
            {
              label: "Open Folder...",
              click: async () => {
                dialog.showOpenDialog({
                  properties: ["openDirectory"]
                }).then(result => {
                  if(result.canceled) return
                  else{
                    console.log(result.filePaths)
                    win.webContents.send("folder-opened", result.filePaths)
                  }
                })
              },
            },
            isMac ? {role: "close"} : {role: "quit"}
        ]
      },
      {
        label: "Dev",
        submenu: [
          {role: "toggleDevTools"}
        ]
      },
      {
        label: "Help",
        submenu: [
          {
            label: 'Learn More',
            click: async () => {
              const { shell } = require('electron')
              await shell.openExternal('https://electronjs.org')
            }
          }
        ]
      }
    ];
  const menu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(menu);
    
  win.loadFile("./src/statics/index.html")
}

app.whenReady().then(() => {
  ipcMain.handle("dialog:openFile", loadFiles)
  createWindow()
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
