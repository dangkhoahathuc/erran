//Import
const {contextBridge, ipcRenderer, Menu} = require("electron");
const path = require("path");
const fs = require("fs/promises");
// preload.js
var fileList = [];
var rootDir = "";
const openingFiles = [];
const templateStore = {
  dataMatrix: document.getElementById("data-matrix")
}
// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
      const element = document.getElementById(selector)
      if (element) element.innerText = text
    }
  
    for (const dependency of ['chrome', 'node', 'electron']) {
      replaceText(`${dependency}-version`, process.versions[dependency])
    }
})

contextBridge.exposeInMainWorld("readFolderAPI", {
  onOpenFolder: () => ipcRenderer.on("folder-opened", (event, args) => {
    rootDir = args[0];
    fs.readdir(args[0]).then(files => {
      fileList = files;
      files.forEach(file => addItemToList("log-list", file));
    });
  })
})

contextBridge.exposeInMainWorld("boilerplateAPI",{
  updateDivContent: (id, content) => {
    const div = document.getElementById("id");
    div.innerHTML = content;
  },
  createTable: (headers) => {
    
  } 
})

function addItemToList(id, content){
  const list = document.getElementById(id);
  const listItem = document.createElement("li");
  listItem.setAttribute("class", "file-list")
  listItem.innerHTML = content;
  listItem.onclick = chooseFileForView;
  list.appendChild(listItem);
}

function chooseFileForView(event){
  const fileName = event.target.innerHTML;
  fs.readFile(path.join(rootDir, fileName))
  .then(content => {
    var contentNode = document.createTextNode(content);
    var textPre = document.createElement("PRE");
    textPre.appendChild(contentNode)
    const contentDiv = document.getElementById("content");
    contentDiv.innerHTML = ""
    contentDiv.appendChild(textPre)
  }).catch(err => {
    console.log(err);
  })
}