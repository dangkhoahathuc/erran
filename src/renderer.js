// Handle resize bar
var m_pos;
var sidebar = document.getElementById("sidebar");
var minWidthSidebar = 200;

function resize(e){
    var dx = m_pos - e.x;
    m_pos = e.x;
    const newWidth = parseInt(getComputedStyle(sidebar, '').width) - dx;
    console.log(newWidth);
    if (newWidth <= minWidthSidebar/2)
        sidebar.style.display  = "none"
    else if(newWidth >= minWidthSidebar){
        sidebar.style.display = "block"
        sidebar.style.width = newWidth + "px";
    }
};

var resize_el = document.getElementById("resize");
resize_el.addEventListener("mousedown", function(e){
    m_pos = e.x;
    document.addEventListener("mousemove", resize, false);
}, false);
document.addEventListener("mouseup", function(){
    document.removeEventListener("mousemove", resize, false);
}, false);

//Handle context menu


//Handle file 
window.readFolderAPI.onOpenFolder();

